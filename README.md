# Battleground
### _3D Arcade-Third Person Shooter Game_

![Title1](/uploads/26ec51af707d4707930b1d243b91db39/Title1.png)

Battleground is a PC Multiplayer Online Arcade Third Person Shooter Game

### Development Team
4210181022 - [Ilham Sugeng Prayogi](https://gitlab.com/IlhamSugengPrayogi17)

4210181030 - [Raka Arya pratama](https://gitlab.com/notslimboy)

### Overview Gameplay
Battleground is an online multiplayer game with the Arcade-Third Person Shooter genre, where players must collect coins up to a certain amount to win, but players will also compete with other players for coins that are randomly spawned on the battlefield. Shooter will be the main mechanic for each player to interfere with the progress of other players to get more coins, by making other players die and making the coins they have collected back to zero

### Document of Detail

High Concept Game : [Battleground](https://docs.google.com/presentation/d/1qv1w462lPm_R11gS_0Mq-WkMAVuSzctbSRvv-UK2Hx0/edit#slide=id.gc4dcfbf537_0_221)

Game Design Document : [GDD](https://docs.google.com/document/d/1LVZFuZOb0D2SCash1Bt0_2hJxKAmFcmL5va6IGfP9oM/edit#heading=h.pv9zxyvats0l)

### Game Documentation

There are several features and functions that are the main foundation of this game, here are the explanations:

#### Packet System

The packet system is the main feature that acts as a bridge between the client and the server to facilitate data exchange, this system has the same function as a multithreaded system which is used to make multiple client systems connected to the same server. The workings of this Packet System is to make the data that you want to send, either from the client or the server will be wrapped in packets, where each packet will have its own identity and destination address. This will make the data can be delivered more accurately and quickly to the destination.

The following are the parts that make up this Packet System so that it can run to become a bridge between multi-clients and servers:

1. Packet Identifier

Packet Identifier has a function to store various kinds of identities that will be used in the process of sending data between the client and server 

Server Side

```
public enum ServerPackets
    {
        welcome = 1,
        login,
        signUp,
        SpawnPlayer,
        playerIsReady,
        totalIsReadyPlayer,
        startGame,
        backLobby,
        spawnCoin,
        destroyCoin,
        playerCoin,
        playerHealth,
        playerRespawned,
        PlayerPosition,
        PlayerRotation,
        spawnProjectile,
        projectilePosition,
        projectileExploded,
        PlayerAnimation,
        winnerPlayer,
        PlayerDisconnected
    }
```

Client Side

```
public enum ClientPackets
    {
        welcomeReceived = 1,
        loginInput,
        signUpInput,
        playerShoot,
        playerThrowProjectil,
        PlayerMovement,
        isReady,
        isBack,
    }
```

2. Packet Handler

Packet Handler is used as the part in charge of receiving every packet that is sent, this Packet Handler will be on the server and client, when it has received a packet with its respective identity, the packet handler will immediately provide access to certain functions to process data from the packet that has been received. 

Server Side

```
packetHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)ClientPackets.welcomeReceived, ServerHandle.WelcomeReceived },
                { (int)ClientPackets.loginInput, ServerHandle.LoginInput },
                { (int)ClientPackets.signUpInput, ServerHandle.SignUpInput },
                { (int)ClientPackets.isReady, ServerHandle.IsReadyInput },
                { (int)ClientPackets.isBack, ServerHandle.IsBackInput },
                { (int)ClientPackets.playerShoot, ServerHandle.PlayerShoot },
                /*{ (int)ClientPackets.playerThrowProjectil, ServerHandle.PlayerThrownProjectile },*/
                { (int)ClientPackets.PlayerMovement, ServerHandle.PlayerMovement },
            };

```

Client Side

```
packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ServerPackets.welcome, ClientHandle.Welcome },
            { (int)ServerPackets.login, ClientHandle.Login },
            { (int)ServerPackets.signUp, ClientHandle.SignUp },
            { (int)ServerPackets.SpawnPlayer, ClientHandle.SpawnPlayer },
            { (int)ServerPackets.playerIsReady, ClientHandle.PlayerIsReady },
            { (int)ServerPackets.totalIsReadyPlayer, ClientHandle.TotalIsReadyPlayer },
            { (int)ServerPackets.startGame, ClientHandle.StartGame },
            { (int)ServerPackets.backLobby, ClientHandle.BackLobby },
            { (int)ServerPackets.spawnCoin, ClientHandle.SpawnCoin },
            { (int)ServerPackets.destroyCoin, ClientHandle.DestroyCoin },
            { (int)ServerPackets.playerCoin, ClientHandle.PlayerCoin },
            { (int)ServerPackets.playerHealth, ClientHandle.PlayerHealth },
            { (int)ServerPackets.playerRespawned, ClientHandle.PlayerRespawned },
            { (int)ServerPackets.PlayerPosition, ClientHandle.PlayerPosition },
            { (int)ServerPackets.PlayerRotation, ClientHandle.PlayerRotation },
            { (int)ServerPackets.projectilePosition, ClientHandle.ProjectilePosition },
            { (int)ServerPackets.projectileExploded, ClientHandle.ProjectileExploded },
            { (int)ServerPackets.spawnProjectile, ClientHandle.SpawnProjectile },
            { (int)ServerPackets.PlayerAnimation, ClientHandle.PlayerAnimation },
            { (int)ServerPackets.winnerPlayer, ClientHandle.WinnerPlayer },
            { (int)ServerPackets.PlayerDisconnected, ClientHandle.PlayerDisconnected },
        };

```

3. Protocol / Data Transfer

This section contains several functions that are used to send data using the TCP or UDP protocol, where both of these protocols are useful for sending data as needed.

```
private static void SendTCPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            Server.clients[_toClient].tcp.SendData(_packet);
        }

        private static void SendUDPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            Server.clients[_toClient].udp.SendData(_packet);
        }

        private static void SendTCPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
        private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != _exceptClient)
                {
                    Server.clients[i].tcp.SendData(_packet);
                }
            }
        }

        private static void SendTCPDataToAllPlayers(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (Server.clients[i].player != null)
                {
                    Server.clients[i].tcp.SendData(_packet);
                }
            }
        }

        private static void SendUDPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
        private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != _exceptClient)
                {
                    Server.clients[i].udp.SendData(_packet);
                }
            }
        }

        private static void SendUDPDataToAllPlayers(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (Server.clients[i].player != null)
                {
                    Server.clients[i].udp.SendData(_packet);
                }
            }
        }
```

#### Authentication

To play this game, players must go through Authentication which is used to check the identity of the player, whether it is registered as a player in this game or not, here is an explanation of the Authentication System in this game which consists of Log In and Sign Up :

1. Log In

a. The first Log In process is that the player must enter data into the provided panel, where this data will be carried by the Login() function in AuthManager, the entered data will be processed into the LoginInput() function in ClientSend

```
public void Login()
    {
        if (usenameinput.text == "")
        {
            statusText.text = "Username must not empty";
            return;
        }
        else if(passwordInput.text == "")
        {
            statusText.text = "Password must not empty";
            return;
        }
        ClientSend.LoginInput(usenameinput.text, passwordInput.text);
    }

```

b. The LoginInput() function will provide an identity for the data sent in the form of a packet, the data sent is a username and password

```
public static void LoginInput(string _username, string _password)
    {
        using (Packet _packet = new Packet((int)ClientPackets.loginInput))
        {
            _packet.Write(_username);
            _packet.Write(_password);

            SendTCPData(_packet);
        }
    }

```
c. The data sent to the server is received by the LoginInput() function in the ServerHandle and the received data is passed to the LoginInput() function in the AuthManager

```
public static void LoginInput(int _fromClient, Packet _packet)
        {
            string _username = _packet.ReadString();
            string _password = _packet.ReadString();
            AuthManager.instance.LoginInput(_fromClient,_username,_password);
        }

```

d. In the loginInput() function in AuthManager this will be used to check whether the data inputted by the player and sent from the client is in accordance with the user data or not, if the data is correct then the data confirmation will be sent again to the client, if the data entered is not included in the user , then a notification to the client if the data sent incorrectly will be sent and the login process cannot be continued. The appropriate data will also activate the SendIntoGame() function where this function will be useful for spawning the player object.

```
public void LoginInput(int _clientID, string _username, string _password)
        {
            User _user = CheckLogin(_username, _password);   
            
            if (authMessageError != AuthMessageError.None)
            {
                string message = "Login Failed";
                switch (authMessageError)
                {
                   case AuthMessageError.WrongPassword:
                        message = "Wrong Password";
                        break;
                    case AuthMessageError.UserNotFound:
                        message = "User Not Found !!!";
                        break;
                }
                ServerSend.Login(_clientID, false,message,_user);
            }
            else
            {
                string message = "Login Succeed";
                
                ServerSend.Login(_clientID, true,message,_user);
                Server.clients[_clientID].SendIntoGame(_user);
            }
        }
```

e. Resend data from the server is sent via the Login() function in ServerSend, where the data sent is the id, message, and data bool with the name _canLogin

```
public static void Login(int _clientID, bool _canLogin, string _message, User _user)
        {
            using (Packet _packet = new Packet((int)ServerPackets.login))
            {
                _packet.Write(_canLogin);
                _packet.Write(_message);

                if (_canLogin)
                {
                    _packet.Write<User>(_user);
                }

                SendTCPData(_clientID, _packet);
            }
        }
```

f. The Login() function in the ClientHandle is in charge of receiving the packet sent and forwarding it to the AuthManager and LobbyManager if the data sent has been successful and the login is successful.

```
public static void Login(Packet _packet)
    {
        bool success = _packet.ReadBool();
        string message = _packet.ReadString();
        
        if (success)
        {
            Clients.instance.user = _packet.ReadObject<User>();
        }

        AuthManager.instance.statusText.text = message;

        AuthManager.instance.authPanel.SetActive(false);
        LobbyManager.instance.lobbyPanel.SetActive(true);
    }
```

#### _Login_

![Login](/uploads/da0b686b64d25cdbce223d924cd0de2d/Login.gif)

2. Sign In

a. If the player does not have an account to log in, then the player can sign up, this process starts from the SignUp() function in AuthManager, where the data entered will be sent via the SignUpInput() function in ClientSend

```
public void SignUp()
    {
        if (usenameinput.text == "")
        {
            statusText.text = "Username must not empty";
            return;
        }
        else if(passwordInput.text == "")
        {
            statusText.text = "Password must not empty";
            return;
        }
        ClientSend.SignUpInput(usenameinput.text, passwordInput.text);
    }
```

b. The SignUpInput() function will send data containing the username and password which will be the data for the new user to the server

```
public static void SignUpInput(string _username, string _password)
    {
        using (Packet _packet = new Packet((int)ClientPackets.signUpInput))
        {
            _packet.Write(_username);
            _packet.Write(_password);

            SendTCPData(_packet);
        }
    }
```

c. Next, the SignUpInput() function in the ServerHandle will receive the packet and then pass it to the SignUpInput() function in the AuthManager

```
public static void SignUpInput(int _fromClient, Packet _packet)
        {
            string _username = _packet.ReadString();
            string _password = _packet.ReadString();
            AuthManager.instance.SignUpInput(_fromClient,_username,_password);
        }
```

d. The SignUpInput() function in AuthManager is to check whether the data used to create a new user is appropriate or not, and also this function will activate another function, namely the CreateNewUser() function in the same script.

```
public void SignUpInput(int _clientID, string _username, string _password)
        {
            CreateNewUser(_username,_password);
            if (authMessageError != AuthMessageError.None)
            {
                string message = "Sign Up Failed";
                switch (authMessageError)
                {
                    case AuthMessageError.UserAlreadyExist:
                        message = "Username Already Existed";
                        break;
                }

                ServerSend.SignUp(_clientID, false, message);
            }
            else
            {
                string message = "Sign Up Succeed";
                ServerSend.SignUp(_clientID, true, message);
            }
        }
```

e. The CreateNewUser() function will create a new user with the data that has been obtained and then add it to the user list

```
public void CreateNewUser(string _username, string _password)
        {
            foreach (User user in users)
            {
                if (_username == user.username)
                {
                    authMessageError = AuthMessageError.UserAlreadyExist;
                    return;
                }
            }

            User _newUser = new User(_username, _password);
            users.Add(_newUser);
            
            authMessageError = AuthMessageError.None;
        }
```

f. When the data for creating a new user is correct and the new user is successfully created, then this information will be given again to the Client via SignUp() on ServerSend, this SignUp() function will send a packet containing id, username, and bool _canLogin to the client

```
public static void SignUp(int _clientID, bool _canLogin, string _message)
        {
            using (Packet _packet = new Packet((int)ServerPackets.signUp))
            {
                _packet.Write(_canLogin);
                _packet.Write(_message);

                SendTCPData(_clientID, _packet);
            }
        }
```

g. The packet will be received by the SignUp() function in the ClientHandle and then forwarded to the AuthManager to give a message if the Sign Up has been successful.

```
public static void SignUp(Packet _packet)
    {
        bool success = _packet.ReadBool();
        string message = _packet.ReadString();
        if (success)
        {
           AuthManager.instance.ResetInput();
        }
        
        AuthManager.instance.statusText.text = message;
    }
```
#### _Sign Up_

![SignUp](/uploads/7f8c2bee7488d348ac3a01c3d7401b14/SignUp.gif)

#### Lobby System

Before entering the game, there is a Lobby System that players will enter before the game takes place, this Lobby System is used to regulate players as well as the process of moving the scene in this game. The lobby system will be the place where players determine whether they are ready to enter the battlefield or not, the game can only start when the lobby system has received data if all players are ready to play, where the lobby will immediately move players who are already in the lobby to the battlefield . Here's an explanation of how the Lobby System works in this game : 

1. Get Total Player

a. The lobby will provide information about how many users have entered the lobby and want to join the game, the GetTotalPlayer() function in the LobbyManager on the server is tasked with increasing the total number of users by re-checking the dictionary client on the server..

```
public int GetTotalPlayer()
        {
            int countPlayer = 0;
            foreach (Client _client in Server.clients.Values)
            {
                if (_client.player != null)
                {
                    countPlayer++;
                }
            }
            return countPlayer;
        }
```

b. GetTotalPlayer() is related to the CheckStartGame() function where in this function the Server will send data about the number of clients that have entered the lobby, and send the data through the TotalIsReadyPlayer() function on ServerSend.

```
public void CheckStartGame()
        {
            int totalPlayer = GetTotalPlayer();
            int isReadyPlayer = GetIsReadyPlayer();
            ServerSend.TotalIsReadyPlayer(totalPlayer,isReadyPlayer);
            if (totalPlayer >= minPlayer && totalPlayer == isReadyPlayer)
            {
                ServerSend.StartGame();
                Debug.Log("Masuk");
                GameManager.instance.StartGame();
            }
        }
```

c. In the ClientHandle, the TotalIsReadyPlayer() function will receive the packet sent and will pass it to the GameUIManager to show the number of players that entered the client UI.

```
public static void TotalIsReadyPlayer(Packet _packet)
    {
        int _totalPlayer = _packet.ReadInt();
        int _totalIsReadyPlayer = _packet.ReadInt();
        GameUIManager.instance.SetTotalPlayer(_totalPlayer);
        GameUIManager.instance.SetReadyPlayer(_totalIsReadyPlayer);
    }
```
#### _Total Player_

![TotalPlayer](/uploads/a4073e91de9c9be9d0e03c3cc9c8be98/TotalPlayer.gif)

2. Get is Ready Player

a. To make the game start, all players must be ready to start the game, this starts from the player entering the lobby and activating the lobby panel, where in this panel there will be a button containing the LobbyManager script, in which there is a ToggleIsReady function ( ) which is used to send data if the Player is ready.

```
public void ToggleIsReady()
    {
        bool isReady = GameManager.instance.players[Clients.instance.myId].isReady;

        ClientSend.isReadyInput(!isReady);
    }
```

b. The IsReadyInput() function in ClientSend will send a packet containing the _isReady bool data to the Server, to make the isReady condition true.

```
public static void isReadyInput(bool _isReady)
    {
        using (Packet _packet = new Packet((int)ClientPackets.isReady))
        {
            _packet.Write(_isReady);

            SendTCPData(_packet);
        }
    }
```

c. ServerHandle will receive packets via the IsReadyInput() function, which will immediately activate several functions, starting from creating a true isReady boolean in the Player, the PlayerIsReady() function will also be active to send data back to the client, and the CheckStartGame() function to check the state of the specified conditions to start the game.

```
public static void IsReadyInput(int _fromClient, Packet _packet)
        {
            bool _isReady = _packet.ReadBool();
            //int _totalReadyPlayer = _packet.ReadInt();
            Debug.Log("Terima");

            Server.clients[_fromClient].player.isReady = _isReady;
            ServerSend.PlayerIsReady(_fromClient, _isReady);

            LobbyManager.instance.CheckStartGame();
        }
```

d. The PlayerIsReady() function will send data in the form of id and bool isReady to the client.

```
public static void PlayerIsReady(int _clientId, bool _isReady)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerIsReady))
            {
                _packet.Write(_clientId);
                _packet.Write(_isReady);

                SendTCPDataToAllPlayers(_packet);
            }
        }
```

e. In the ClientHandle, the PlayerIsready() function will receive the packet and forward it to the GameManager and then to the Player Manager to provide data if the player with the associated user id is in ready condition.

```
public static void PlayerIsReady(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _isReady = _packet.ReadBool();

        GameManager.instance.players[_id].isReady = _isReady;
        //GameUIManager.instance.SetReadyPlayer(_id);
    }
```

f. When a player enters the lobby, in addition to the GetTotalPlayer() function, there is another function, namely GetIsreadyPlayer(), where this function is useful for checking the number of players who are already in the ready position.

```
public int GetIsReadyPlayer()
        {
            int countIsReadyPlayer = 0;
            foreach (Client _client in Server.clients.Values)
            {
                if (_client.player != null && _client.player.isReady)
                {
                    countIsReadyPlayer++;
                    Debug.Log("PlayerCounter: " + countIsReadyPlayer);
                    //ServerSend.TotalIsReadyPlayer(countIsReadyPlayer);
                }
            }
            return countIsReadyPlayer;
            //ServerSend.PlayerIsReady();
        }
```

3. Start Game and go to battlefield

a. As explained earlier, if the game will only start if all players who have entered the lobby must be in a ready condition, therefore in the LobbyManager script there is a CheckStartGame() function which in this function is when all players in the lobby are in a state ready, then some functions will be active.

```
public void CheckStartGame()
        {
            int totalPlayer = GetTotalPlayer();
            int isReadyPlayer = GetIsReadyPlayer();
            ServerSend.TotalIsReadyPlayer(totalPlayer,isReadyPlayer);
            if (totalPlayer >= minPlayer && totalPlayer == isReadyPlayer)
            {
                ServerSend.StartGame();
                Debug.Log("Masuk");
                GameManager.instance.StartGame();
            }
        }
```

b. The first function that is active is StartGame() on ServerSend, this function will send data when the game is ready to be played to the client.

```
public static void StartGame()
        {
            using (Packet _packet = new Packet((int)ServerPackets.startGame))
            {
                SendTCPDataToAllPlayers(_packet);
            }
        }
```

c. On the clientHandle, the StartGame() function will receive the packet and continue its process to the LoadScene() function on the SceneManager, where all players will move the scene to the battlefield or in here is the Residental 1 scene, and the game will start immediately when the player has entered the scene.

```
public static void StartGame(Packet _packet)
    {
        Debug.Log("Start Game");
        SceneManager.LoadScene("Residental 1");
    }
```

d. In addition to the StartGame() function in GameManager, the StartGame() function in GameManager will be active where this function will create spawn coins on the active server

```
public void StartGame()
        {
            coinSpawner.isSpawning = true;
        }
```
#### _Player Ready & Game Start_

![ezgif.com-gif-maker](/uploads/cda8812c60ade9a7e71822c713a0eee5/ezgif.com-gif-maker.gif)

#### Main Gameplay

There are several main mechanism in this game, here's an explanation of how all these mechanisms work in this online multiplayer game :

##### 1. Spawning Player

###### - This Feature process

1. Along with the successful Authentication process, the incoming player data will be sent to the SendIntoGame() function in the client script on the server, in this SendIntoGame() function the data about the player id and username will be used again to perform the player spawn process by activating the SpawnPlayer() on ServerSend.

```
public void SendIntoGame(User _user)
        {
            user = _user;
            player = NetworkManager.instance.SpawnPlayer();
            player.Initialize(id, _user.username, new Vector3(0, 0, 0));

            foreach (Client _client in Server.clients.Values)
            {
                if (_client.player != null)
                {
                    if (_client.id != id)
                    {
                        ServerSend.SpawnPlayer(id, _client.player);
                    }
                }
            }

            foreach (Client _client in Server.clients.Values)
            {
                if (_client.player != null)
                {
                    ServerSend.SpawnPlayer(_client.id, player);
                }
            }
            int totalPlayer = LobbyManager.instance.GetTotalPlayer();
            int countIsReadyPlayer = LobbyManager.instance.GetIsReadyPlayer();
            ServerSend.TotalIsReadyPlayer(totalPlayer,countIsReadyPlayer);
        }
```

2. To perform the Spawn Player process, the SendIntoGame() function will take the SpawnPlayer() function in the NetworkManager script which stores data about the Player object, starting from the prefab, position and others.

```
public Player SpawnPlayer()
        {
            Player player = Instantiate(PlayerPrefabs, respawnPosition.position, Quaternion.identity).GetComponent<Player>();
            player.transform.SetParent(this.transform, false);
           
            return player;
        }
```

3. In Server Player will be spawned, along with this process, data about id, username, position and rotation will be sent via packet via PlayerSpawn() function in ServerSend

```
public static void SpawnPlayer(int _toClient, Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.SpawnPlayer))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.username);
                _packet.Write(_player.transform.position);
                _packet.Write(_player.transform.rotation);

                SendTCPData(_toClient, _packet);
            }
        }
```

4. The client will receive the package via the ClientHandle, after getting the package the SpawnPlayer() function in GameManager will be active

```
public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();

        GameManager.instance.SpawnPlayer(_id, _username, _position, _rotation);
    }
```

5. The SpawnPlayer() function in GameManager will be used to spawn for players on the Client

```
public void SpawnPlayer(int _id, string _username, Vector3 _position, Quaternion _rotation)
    {
        GameObject _player;
        if (_id == Clients.instance.myId)
        {
            _player = Instantiate(localPlayerPrefab, _position, _rotation);
        }
        else
        {
            _player = Instantiate(playerPrefab, _position, _rotation);
        }

        _player.transform.SetParent(this.transform, false);
        _player.GetComponent<PlayerManager>().Initialize(_id, _username);
        players.Add(_id, _player.GetComponent<PlayerManager>());
    }
```

##### 2. Player Movement

###### - This Feature process

1. The client gets input from the player, the SendInputToServer() function in the PlayerController becomes the initial stage for the player movement process, where this function will receive input from the player and the input results will relate to the PlayerMovement() function in the ClientSend script.

```
private void SendInputToServer()
    {
        bool[] _inputs = new bool[]
        {
            Input.GetKey(KeyCode.W),
            Input.GetKey(KeyCode.A),
            Input.GetKey(KeyCode.S),
            Input.GetKey(KeyCode.D)
        };

        ClientSend.PlayerMovement(_inputs);
    }
```

2. The PlayerMovement() function in ClientSend will send the input results to the server in the form of a packet, which contains the input made by the player, this function will send packets using the UDP protocol

```
public static void PlayerMovement(bool[] _inputs)
    {
        using (Packet _packet = new Packet((int)ClientPackets.PlayerMovement))
        {
            _packet.Write(_inputs.Length);
            foreach (bool _input in _inputs)
            {
                _packet.Write(_input);
            }
            _packet.Write(GameManager.instance.players[Clients.instance.myId].transform.rotation);

            SendUDPData(_packet);
        }
    }
```

3. The ServerHandle will receive the packet via the PlayerMovement() function, this function will read the contents of the received packet and forward it to the Player script via the SetInput() function.

```
public static void PlayerMovement(int _fromClient, Packet _packet)
        {
            bool[] _inputs = new bool[_packet.ReadInt()];
            for (int i = 0; i < _inputs.Length; i++)
            {
                _inputs[i] = _packet.ReadBool();
            }
            Quaternion _rotation = _packet.ReadQuaternion();

            Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
        }
```

4. The SetInput() function will get the data inputted by the player, and use it in the player script to update the position depending on the input data received.

```
public void SetInput(bool[] _inputs, Quaternion _rotation)
        {
            inputs = _inputs;
            transform.rotation = _rotation;
        }
```

5. The input will enter the update function and check directly which input is made by the player, after passing the update, the data will be continued to the Move() function where this function will update the position and rotation of the player according to the input results.

```
private void Update()
        {
            if (health <= 0f)
            {
                return;
            }

            Vector2 _inputDirection = Vector2.zero;
            if (inputs[0])
            {
                _inputDirection.y += 1;
            }
            if (inputs[1])
            {
                _inputDirection.x -= 1;
            }
            if (inputs[2])
            {
                _inputDirection.y -= 1;
            }
            if (inputs[3])
            {
                _inputDirection.x += 1;
            }

            Move(_inputDirection);
        }

        private void Move(Vector2 _inputDirection)
        {
            if (canWalk)
            {
                Vector3 _moveDirection = transform.right * _inputDirection.x + transform.forward * _inputDirection.y;
                transform.position += _moveDirection * moveSpeed;
                PlayerAnim.SetBool("isWalk",true);
                ServerSend.SendPlayerAnimation(id, "isWalk",true);
            }
            
            if (_inputDirection == Vector2.zero)
            {
                    
                    PlayerAnim.SetBool("isWalk",false);
                    PlayerAnim.SetBool("isIdle",true);
                    ServerSend.SendPlayerAnimation(id, "isWalk",false);
                    ServerSend.SendPlayerAnimation(id, "isIdle",true);
            }
                

            ServerSend.PlayerPosition(this);
            ServerSend.PlayerRotation(this);
        }
```

6. These position and rotation updates will be sent via the PlayerPosition() and PlayerRotation() functions on ServerSend, both of which will send the id and update transform to the client.

```
public static void PlayerPosition(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.PlayerPosition))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.transform.position);

                SendUDPDataToAllPlayers(_packet);
            }
        }

        public static void PlayerRotation(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.PlayerRotation))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.transform.rotation);

                SendUDPDataToAllPlayers(_packet);
            }
        }
```

7. Both functions that have the same name in the ClientHandle will receive the packet and then continue the process to the update transform in GameManager, and make the position and rotation of the player object on the client also updated. 

```
public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Vector3 _position = _packet.ReadVector3();

            GameManager.instance.players[_id].transform.position = _position;
        }
    }

    public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Quaternion _rotation = _packet.ReadQuaternion();

            GameManager.instance.players[_id].transform.rotation = _rotation;
        }
    }
```

##### 3. Coin Spawn

###### - This Feature process

1. The CoinSpawn script will be the main script used to manage the Spawn coin process, this script only works when the StartGame() function in the GameManager script is active. When the CoinSpawn script is active, the coin spawn process will run by taking the provided prefabs. In addition to taking prefabs, this script is also used to determine the number of coins that are spawned, the spawn position of each coin, and the spawn deadline between coins. CoinSpawn will also send the id and position of each coin spawned via ServerSend

```
public class CoinSpawn : MonoBehaviour
    {
        public static Dictionary<int, Coin> coins = new Dictionary<int, Coin>();
        private int idCount;

        public Vector3 spawnValues;
        
        public float spawnMostWait;
        public float spawnLeastWait;
        private float spawnWait;

        public bool isSpawning;

        [Header("Prefab")]
        public Coin coinPrefab;

        private void Start()
        {
            spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
        }

        private void Update()
        {
            if (!isSpawning) return;
            
            if (spawnWait > 0)
            {
                spawnWait -= Time.deltaTime;
            } 
            else
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), 1, Random.Range(-spawnValues.z, spawnValues.z));
                spawnPosition += this.transform.position;

                ServerSend.SpawnCoin(idCount, spawnPosition);
                Coin coin = Instantiate(coinPrefab, spawnPosition, coinPrefab.transform.rotation) as Coin;
                
                coin.Initialize(idCount);
                idCount++;

                coins.Add(idCount, coin);

                spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
            }

        }
    }
```

2. On ServerSend in the SpawnCoin() function the coin data i.e. id and position are sent via packet to the Client

```
public static void SpawnCoin(int _coinId, Vector3 _position)
        {
            using (Packet _packet = new Packet((int)ServerPackets.spawnCoin))
            {
                _packet.Write(_coinId);
                _packet.Write(_position);
                SendTCPDataToAllPlayers(_packet);
            }
        }
```

3. The client receives the packet on the ClientHandle and activates the SpawnCoin() function in the GameManager

```
public static void SpawnCoin(Packet _packet)
    {
        int _coinId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.SpawnCoin(_coinId, _position);
    }
```

4. The SpawnCoin() function in GameManager will instantiate the prefab and spawn coin according to the data on the id and position from the Server 

```
public void SpawnCoin(int _coinId, Vector3 _position)
    {
        Coin coin = Instantiate(coinPrefab, _position, coinPrefab.transform.rotation) as Coin;
        coin.Initialize(_coinId);
        coins.Add(_coinId, coin);
    }
```

##### 4. Player Shooting

###### - This Feature process

1. Shooting starts from the client receiving input from the player in the Update() function in the PlayerController Script, which is directly used to activate the PlayerShoot() function in ClientSend.

```
private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            ClientSend.PlayerShoot(camTransform.forward);
        }
    }
```

2. In the PlayerShoot() function in ClientSend the existing facing data is sent to the server in the form of Packets using the TCP protocol media

```
public static void PlayerShoot(Vector3 _facing)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerShoot))
        {
            _packet.Write(_facing);

            SendTCPData(_packet);
        }
    }
```

3. The PlayerShoot() function in the ServerHandle is in charge of receiving packets from the server, and proceeds to the Shoot() and ThrowItem() functions in the Player script.

```
public static void PlayerShoot(int _fromClient, Packet _packet)
        {
            Vector3 _shootDirection = _packet.ReadVector3();

            Server.clients[_fromClient].player.Shoot(_shootDirection);
            Server.clients[_fromClient].player.ThrowItem(_shootDirection);
        }
```

4. The Shoot() function is used to activate animations and inflict damage on other player objects in the direction of the player's camera

```
public void Shoot(Vector3 _viewDirection)
        {
            if (health <= 0)
            {
                return;
            }

            if (Physics.Raycast(shootOrigin.position, _viewDirection, out RaycastHit _hit, 25f))
            {
                PlayerAnim.SetTrigger("isShoot");
                ServerSend.SendPlayerAnimation(id,"isShoot",false);
                if (_hit.collider.CompareTag("Player"))
                {
                    _hit.collider.transform.parent.gameObject.GetComponent<Player>().TakeDamage(50f);
                }
            }    
        }
```

5. As for ThrowItem(), it immediately activates the InstantiateProjectile() function in the NetworkManager script to spawn a projectile

```
public void ThrowItem(Vector3 _viewDirection)
        {
            NetworkManager.instance.InstantiateProjectile(shootOrigin).Initialize(_viewDirection, throwForce, id);
        }
```

6. The Instantiate Projectile() function which is connected to the projectile script, is used to instantiate data from the projectile starting from prefab, position and quaternion

```
public Projectil InstantiateProjectile(Transform _shootOrigin)
        {
            return Instantiate(projectilePrefab, _shootOrigin.position + _shootOrigin.forward * 0.7f, Quaternion.identity).GetComponent<Projectil>();
        }
```

7. In the projectil script there will be several active functions, starting from the Start() function which will spawn the projectile and send data to the client via ServerSend

```
private void Start()
        {
            id = nextProjectileId;
            nextProjectileId++;
            projectiles.Add(id, this);

            ServerSend.SpawnProjectile(this, thrownByPlayer);

            rigidBody.AddForce(initialForce);
            StartCoroutine(ExplodeAfterTime());
        }
```

8. Next is the FixedUpdate() function which will update the position of the projectile that has been shot and send the data via ServerSend so that the position on the Client will also updated.

```
private void FixedUpdate()
        {
            ServerSend.ProjectilePosition(this);
        }
```

9. Then there is the Explode() function which is active when the projectile hits an object with the "Player" tag, it will damage the player object, as well as activate the explosion effect. All this data will also be sent via ServerSend to be updated on the Client

```
private void Explode()
        {
            ServerSend.ProjectileExploded(this);

            Collider[] _colliders = Physics.OverlapSphere(transform.position, explosionRadius);
            foreach (Collider _collider in _colliders)
            {
                if (_collider.CompareTag("Player"))
                {
                    //_collider.GetComponent<Player>().TakeDamage(explosionDamage);
                    _collider.transform.parent.gameObject.GetComponent<Player>().TakeDamage(25f);
                }
            }

            Destroy(gameObject);
        }
```

10. In the ServerSend script, the three functions above will send packets via the TCP protocol

```
public static void SpawnProjectile(Projectil _projectile, int _thrownByPlayer)
        {
            using (Packet _packet = new Packet((int)ServerPackets.spawnProjectile))
            {
                _packet.Write(_projectile.id);
                _packet.Write(_projectile.transform.position);
                _packet.Write(_thrownByPlayer);

                SendTCPDataToAll(_packet);
            }
        }

        public static void ProjectilePosition(Projectil _projectile)
        {
            using (Packet _packet = new Packet((int)ServerPackets.projectilePosition))
            {
                _packet.Write(_projectile.id);
                _packet.Write(_projectile.transform.position);

                SendTCPDataToAll(_packet);
            }
        }

        public static void ProjectileExploded(Projectil _projectile)
        {
            using (Packet _packet = new Packet((int)ServerPackets.projectileExploded))
            {
                _packet.Write(_projectile.id);
                _packet.Write(_projectile.transform.position);

                SendTCPDataToAll(_packet);
            }
        }
```

11. All packets of the three functions will be received in the ClientHandle according to their respective data recipients, where everything will be directly connected to the GameManager

```
public static void SpawnProjectile(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        int _thrownByPlayer = _packet.ReadInt();

        GameManager.instance.SpawnProjectile(_projectileId, _position);
    }

    public static void ProjectilePosition(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.projectiles[_projectileId].transform.position = _position;
    }

    public static void ProjectileExploded(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.projectiles[_projectileId].Explode(_position);
    }
```

12. The ProjectileExploded() function will be connected to the Explode() function in the ProjectileManager script, where the Explode() function will function to instantiate the Explode effect, and destroy the object..

```
public void Explode(Vector3 _position)
    {
        transform.position = _position;
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
```

13. In the SpawnProjectile() function in GameManager, there will be an Instantiate on the Projectile object and the Client can finally shoot and spawn the projectile

```
public void SpawnProjectile(int _id, Vector3 _position)
    {
        GameObject _projectile = Instantiate(projectilePrefab, _position, Quaternion.identity);
        _projectile.GetComponent<ProjectileManager>().Initialize(_id);
        projectiles.Add(_id, _projectile.GetComponent<ProjectileManager>());
    }
```

#### _Player Shoot_

![PlayerShoot__1_](/uploads/c9e0e2a781919c434bef19cbe1762b88/PlayerShoot__1_.gif)

##### 5. Player Coin

###### - This Feature process

1. Player coin is a system that allows players to get coins after colliding with coin objects in Battlefield, this system starts from the OnTriggerEnter() function in the coin script on the server, where if the coin object collides with an object with the "Player" tag then the function AddCoins() in Player script will be active.

```
private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                other.transform.parent.gameObject.GetComponent<Player>().AddCoins();
                DestroyCoin();
            }
        }
```

2. This AddCoins() function will add the number of coins owned by the Player to the stats, as well as send the latest data from the number of coins via the PlayerCoin() function on ServerSend.

```
public void AddCoins()
        {
            coin++;
            ServerSend.PlayerCoin(id, coin);

            if (coin == 10)
            {
                ServerSend.WinnerPlayer(id);
                coin = zeroCoin;
            }
        }
```

3. The PlayerCoin() function itself has a function to send data from updating the number of coins owned by the player, so that the Player object in the Client also gets the same update, where the packet sent via the PlayerCoin() function contains the player id and the latest coin number from the player.

```
public static void PlayerCoin(int _toClient, int _coinAmount)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerCoin))
            {
                _packet.Write(_coinAmount);
                SendTCPData(_toClient, _packet);
            }
        }
```

4. PlayerCoin() in the ClientHandle will receive the packet sent from the server and directly connect it to the GameManager to update the number of coins on the player with the appropriate id.

```
public static void PlayerCoin(Packet _packet)
    {
        int _coinAmount = _packet.ReadInt();

        GameManager.instance.players[Clients.instance.myId].coin = _coinAmount;
        GameUIManager.instance.SetCoin(_coinAmount);
    }
```

5. In addition to going to GameManager, to display the number of coins in the UI, the data that has been obtained by PlayerCoin() will also be passed to the SetCoin() function in GameUIManager.

```
public void SetCoin(int _coinAmount)
    {
        coinText.text = "Coin : " + _coinAmount;
    }
```
#### _Player Coin_

![PlayerCoin__1_](/uploads/451e666db5d5ca442df88219a340b921/PlayerCoin__1_.gif)

##### 6. Player Health

###### - This Feature process

1. Player Health is a system that is used to regulate the amount of health of the Player after being hit by a projectile, therefore this feature starts from the Explode() function in the projectil script on the server side, where in that function, if the projectile collides with the object tag "Player", then the TakeDamage() function will be active.

```
private void Explode()
        {
            ServerSend.ProjectileExploded(this);

            Collider[] _colliders = Physics.OverlapSphere(transform.position, explosionRadius);
            foreach (Collider _collider in _colliders)
            {
                if (_collider.CompareTag("Player"))
                {
                    //_collider.GetComponent<Player>().TakeDamage(explosionDamage);
                    _collider.transform.parent.gameObject.GetComponent<Player>().TakeDamage(25f);
                }
            }

            Destroy(gameObject);
        }
```

2. TakeDamage() itself will set the health on the player after being hit by the projectile, the number of health will be updated and the data will be sent to the Client via the PlayerHealth() function on ServerSend.

```
public void TakeDamage(float _damage)
        {
            if (health <= 0f)
            {
                return;
            }

            health -= _damage;
            if (health <= 0f)
            {
                health = 0f;
                transform.position = NetworkManager.instance.respawnPosition.position;
                ServerSend.PlayerPosition(this);
                StartCoroutine(PlayerRespawn());
            }

            ServerSend.PlayerHealth(this);
        }
```

3. PlayerHealth() will send health update data to the client by sending a packet containing the latest health amount and client id.

```
public static void PlayerHealth(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerHealth))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.health);

                SendTCPDataToAll(_packet);
            }
        }
```

4. The PlayerHealth() function on the ClientHandle will receive a packet from the server, and will continue to update the health data on the player with the appropriate id via GameManager.

```
public static void PlayerHealth(Packet _packet)
    {
        int _id = _packet.ReadInt();
        float _health = _packet.ReadFloat();
        
        GameManager.instance.players[_id].SetHealth(_health);
        if (_id == Clients.instance.myId)
        { 
            GameManager.instance.players[Clients.instance.myId].health = _health;
            GameUIManager.instance.SetHealth(_health);
        }
        
    }
```

5. Update data from health will also be used by the SetHealth() function in GameUIManager to display the latest health data in the Ui Client.

```
public void SetHealth(float _healthAmount)
    {
        healthText.text = "Health : " + _healthAmount;
    }
```
#### _Player Health_

![ezgif.com-gif-maker__1_](/uploads/8d4d0e9ddb60baa55aa5481e7eb4aa49/ezgif.com-gif-maker__1_.gif)

##### 7. Die & Respawn System

###### - This Feature process

1. The Die & Respawn System starts from the TakeDamage() function in the Player script on the server side, in the TakeDamage() function when the health of the player has reached a value less than equal to 0, then the position of the player will be returned to the spawn spot, and position updates This is sent to the client via the PlayerPosition() function on ServerSend, the health update will also be sent via the PlayerHealth() function.

```
public void TakeDamage(float _damage)
        {
            if (health <= 0f)
            {
                return;
            }

            health -= _damage;
            if (health <= 0f)
            {
                health = 0f;
                transform.position = NetworkManager.instance.respawnPosition.position;
                ServerSend.PlayerPosition(this);
                StartCoroutine(PlayerRespawn());
            }

            ServerSend.PlayerHealth(this);
        }
```

2. Same as sending packets for Player health before, here sending packets via the PlayerHealth() function which contains the id and health updates.

```
public static void PlayerHealth(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerHealth))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.health);

                SendTCPDataToAll(_packet);
            }
        }
```

3. In addition to PlayerHealth(), the PlayerPosition() function will also send a packet to the client with the contents of id and transform.position, after sending the position, this function will also coroutine the PlayerRespawn() function.

```
public static void PlayerPosition(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.PlayerPosition))
            {
                _packet.Write(_player.id);
                _packet.Write(_player.transform.position);

                SendUDPDataToAllPlayers(_packet);
            }
        }
```

4. The PlayerRespawn() function on this server respawns the player object with the initial default stats, and the data is also sent to the client via the PlayerRespawned() function on ServerSend.

```
private IEnumerator PlayerRespawn()
        {
            yield return new WaitForSeconds(5f);

            health = maxHealth;
            coin = zeroCoin;
            ServerSend.PlayerRespawned(this);
        }
```

5. PlayerRespawned() will send data in the form of id from player to client.

```
public static void PlayerRespawned(Player _player)
        {
            using (Packet _packet = new Packet((int)ServerPackets.playerRespawned))
            {
                _packet.Write(_player.id);

                SendTCPDataToAll(_packet);
            }
        }
```

6. Back to the health packet, this packet is received by the PlayerHealth() function which will be directly connected to the GameManager and GameUIManager.

```
public static void PlayerHealth(Packet _packet)
    {
        int _id = _packet.ReadInt();
        float _health = _packet.ReadFloat();
        
        GameManager.instance.players[_id].SetHealth(_health);
        if (_id == Clients.instance.myId)
        { 
            GameManager.instance.players[Clients.instance.myId].health = _health;
            GameUIManager.instance.SetHealth(_health);
        }
        
    }
```

7. In GameManager, health which is already in the value 0, will enter the SetHealth function in the PlayerManager script, because the health value is equal to 0 it will activate the Die() function

```
public void SetHealth(float _health)
    {
        health = _health;
        if (id == Clients.instance.myId)
        {
            GameUIManager.instance.SetHealth(health);
        }

        if (health <= 0)
        {
            Die();
        }
    }
```

8. The Die() function has a function to remove the player object because its health is already 0.

```
public void Die()
    {
        model.enabled = false;
    }
```

9. Returning to Player Respawn, the data from the Respawn packet is received by the PlayerRespawned() function in the ClientHandle, and directly passed to the GameManager connected to the Respawn() function in the PlayerManager script.

```
public static void PlayerRespawned(Packet _packet)
    {
        int _id = _packet.ReadInt();

        GameManager.instance.players[_id].Respawn();
    }
```

10. This Respawn() function is used to reactivate the previously lost player object, return its stats back to default, and because previously the player's position has been reset to the initial position with the PlayerPosition() function which has received new data from the Server, then the respawn position is also in the initial position

```
public void Respawn()
    {
        model.enabled = true;
        SetCoin(zeroCoin);
        //GameUIManager.instance.SetCoin(id);
        SetHealth(maxHealth);
    }
```
#### _Player Health_

![ezgif.com-gif-maker__2_](/uploads/a41d883fd11c60710cc0f09d9f1a104d/ezgif.com-gif-maker__2_.gif)

##### 8. Win-Lose System

###### - This Feature process

1. Win-Lose system starts from the AddCoins() function on the Server, where in this function if the player's coin is equal to a certain amount (here 10) it will activate the WinnerPlayer() function on ServerSend

```
public void AddCoins()
        {
            coin++;
            ServerSend.PlayerCoin(id, coin);

            if (coin == 10)
            {
                ServerSend.WinnerPlayer(id);
                coin = zeroCoin;
            }
        }
```

2. The WinnerPlayer() function will send the id data from the player on the server to the client who has received 10 coins via TCP protocol to all clients

```
public static void WinnerPlayer(int _clientId)
        {
            using (Packet _packet = new Packet((int)ServerPackets.winnerPlayer))
            {
                _packet.Write(_clientId);

                SendTCPDataToAll(_packet);
            }
        }
```

3. Data from this server will be received in packet form by WinnerPlayer() function in ClientHandle and directly connected to ShowWinner() function in GameUIManager

```
public static void WinnerPlayer(Packet _packet)
    {
        int winnerID = _packet.ReadInt();

        GameUIManager.instance.ShowWinner(winnerID);
    }
```

4. The ShowWinner() function has a function if the received id matches with player id then the Winner panel will be active, while for other players with a different id, the lose panel will be active

```
public void ShowWinner(int _winnerId)
    {
        if (Clients.instance.myId == _winnerId)
        {
            winCanvas.SetActive(true);
        }
        else
        {
            loseCanvas.SetActive(true);
        }
    }
```
#### _Player Win & Lose_

![Win](/uploads/84c589e3d84c671b3f0a52733ec2c295/Win.gif)











